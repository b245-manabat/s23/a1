



let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(){
            console.log(this.pokemon[0]+"! I choose you!")
    }
}

console.log(trainer);


console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);


console.log("Result of talk method:");
trainer.talk();


function Pokemon(nameAdd,levelAdd,healthAdd,attackAdd,tackle) {
    this.Name = nameAdd,
    this.Level = levelAdd,
    this.Health = healthAdd,
    this.Attack = attackAdd,
    
    this.tackle = function(targetPokemon){
                if(targetPokemon.Health>0){
                    console.log(this.Name + " tackles " + targetPokemon.Name);
                                
                    let hpAftertackle = targetPokemon.Health- this.Attack;
                    targetPokemon.Health = hpAftertackle;
                                
                    console.log(targetPokemon.Name + " health is now reduced to " + hpAftertackle);

                        targetPokemon.faint = function(){
                            if(targetPokemon.Health<1){
                                console.log(targetPokemon.Name + " fainted!");
                            }
                            }

                    targetPokemon.faint();
                    console.log(targetPokemon);

                    }

        }
}

    let pikachu = new Pokemon("Pikachu", 12, 24, 12);
    console.log(pikachu)
    let geodude = new Pokemon("Geodude", 8, 16, 8);
    console.log(geodude)
    let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);
    console.log(mewtwo)

    geodude.tackle(pikachu);

    mewtwo.tackle(geodude);

